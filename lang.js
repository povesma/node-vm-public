var fs = require("fs");

function load(fn) {
  return fs.readFileSync(fn);
}
String.prototype.splitCSV = function() {
        let holder = '#$#$#$#$#$#$#$#$#';
        var matches = this.split('""').join(holder).match(/(\s*"[^"]+"\s*|\s*[^,]+|,)(?=,|$)/g);
        for (var n = 0; n < matches.length; ++n) {
            matches[n] = matches[n].trim().split(holder).join('"');
            if (matches[n] == ',') matches[n] = '';
        }
        if (this[0] == ',') matches.unshift("");
        return matches;
}

var s = load("lang.csv").toString();
s = s.split("\r").join("\n");
s = s.split("\n\n").join("\n");
s = s.split("\n\n").join("\n");
var rows = s.split("\n");

var ru = [];
var en = [];
var uk = [];
var src = [];
var json = [];
rows.forEach( (el) => { 
  
  if (el.endsWith(',')) {
    el = el.substr(0,el.length-1);
  }
  let a = el.splitCSV().map((el) => {return el.replace(/^\"(.+)\"$/,'$1');});
  if (a.length >= 3) {
    console.log("Good: ", a);
    ru.push(a[0]);
    en.push(a[1]);
    uk.push(a[2]);
    src.push(a[0]);
    json.push({src:a[0], ru:a[0], en:a[1], uk: a[2]});
  } else {
    console.log("---------- EMPTY: ", a);
  }
});

fs.writeFileSync('db.json', JSON.stringify(json,null, 1));

fs.writeFileSync('ru.txt', ru.join('\n'));
fs.writeFileSync('en.txt', en.join('\n'));
fs.writeFileSync('uk.txt', uk.join('\n'));
