// jump.js - redirection standalone server
var wait = require('wait.for');
rd = require('./redirector.js');
r = new rd();
wait.launchFiber(r.initDb, null);
console.log("-------"+r.makeid(5));
var express = require('express');
var format = require('util').format;
var app = express();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json({ limit:'1MB', extended: false });
process.env.TZ = 'Pacific/Auckland';

app.get('/go/:id', r.redirect);
app.get('/:id', r.redirect);


app.post('/post', jsonParser, function(req,res) {
	wait.launchFiber(r.redirectpost, req, res); //handle in a fiber, keep node spinning
});
var server = app.listen(1608, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Redirector listening at http://%s:%s", host, port)
});


