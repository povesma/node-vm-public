// 'use strict';
var assert = require('assert');
var wait = require('wait.for');
var request = require('request');
var format = require('util').format;
var MongoClient = require('mongodb').MongoClient
//var db = null; -> moving this to the instance

const mongoConnectRetries = 1000;
const mongoPoolSize = 10;
function redirect (g, url, tz) { // url = mongodb url. Mandatrory!
    if (!url) {
        throw new Error('mongodb_url not set in config! '+url);
    }
    if (g) {
        global = g;
    }

    this.initDb =initDb;
    function initDb (dbconn) {
        if (!dbconn) {
            // Connection URL
            // Use connect method to connect to the server
            dbconn = wait.for(MongoClient.connect, url, {reconnectTries:  mongoConnectRetries, poolSize: mongoPoolSize}); 
            if (dbconn != null) {
                console.log("New DB connection made to " + url);
                db = dbconn;
            }
        } else {
            db = dbconn;
            console.log("DB connection reused");
        }
        return dbconn;
    }


    this.makeid = makeid;
    function makeid(len) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < len; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    this.redirect = redirect;
    function redirect (req, res) {
        if(req.params.id!=undefined){
            var c = db.collection("links");
            var clicks = db.collection("clicks");
            console.log("Looking for "+req.params.id);
            req.params.id = req.params.id.split('.')[0] // if it has dot - discard everything after dot
            c.find({id:req.params.id}).toArray(function(err, docs) {
                    var ll = docs.length;
                    console.log(format('docs: %j, len: %s', docs, ll));
                    //assert(docs, null);
                    if (ll>0) {
                        docs.every(function (e,i) {
                            //TODO: if shortcut is short and not public - ask for authentication
                            if (e.redirect.length>5) {
                                // TODO: Check if it was paypal link and callcack Paypal module (to track the purchase process?)
                                if(e.type=="url") {
                                    // e.redirect = '<html><head><meta property="og:url" content="'+e.redirect+'"/>\n<meta http-equiv="refresh" content="1;url='+e.redirect+'"></head></html>'
                                    res.redirect(301, e.redirect);
                                } 
                                if(e.type=="refresh") {
                                    var to_send = '<html><head><meta property="og:url" content="'+e.redirect+'"/>\n<meta http-equiv="refresh" content="1;url='+e.redirect+'"></head></html>'
                                    res.end(to_send);
                                } 
                                if (e.type=="post") {// ... and leave post as it is
                                    res.end(e.redirect);
                                }
                                if (e.type=="proxy") {// ... pipe target URL to the user directly (act as a proxy)
                                    try {
                                        var reply = request(e.redirect);
                                        //req.pipe(reply);
                                        var in_pipe = req.pipe(reply)
                                        in_pipe.on('error', function (ex) {
                                            console.log("pipe PROXY ERROR: ", e);
                                        });
                                        
                                        in_pipe.pipe(res);
                                        //res.end();
                                    } catch (e) {
                                        console.log("PROXY ERROR: ", e);
                                    }
                                }
                                var headers = req.headers;
                                //headers.cookies = req.cookies;  // cookies are stored with headers
                                headers.hostname = req.hostname;
                                headers.ip = req.ip;
                                headers.ips = req.ips; // proxy delivered IPs
                                headers.method = req.method;
                                headers.ip = req.ip;

                                console.log(format("Headers: %j, ", headers));
                                clicks.insert({"id":req.params.id, "type":e.type, "link":e.redirect, "headers":headers, "timestamp":new Date().getTime(), "time":new Date()});
                                // TODO: record headers to DB
                                console.log(format("%s redirected to %s, type: %s", req.params.id, e.redirect, e.type));
                            } else {
                                console.log("Wrong redirect entry for this ("+req.params.id+") id");
                                res.status(404).send("Pff. Wrong URL.");
                            }
                        });
                    } else {
                        console.log("Redirect entry not found for this ("+req.params.id+") id");
                        res.status(404).send("Wrong shortcut.");
                    }
            });
        } else {
            res.status(501).send('I need id!');
        }

    }

    this.redirectpost = redirectpost;

    function redirectpost (req, res) {
        console.log("Data: %j, type of req: %s",req.body, typeof(req));
        var f = {short:shortify, result: 'empty'};
        var type = req.body.type; // url or form
        var url = req.body.url; // 
        var post = req.body.post; // has priority
        var go = "";
        if (url != undefined) {
            go = url;
            type = "url";
        }
        if(post != undefined) {
            go = post; // user must post a form here
            type = "post";
        }
        if (go != undefined && go!="") {
            var startTime = new Date().getTime();
            var rrr = wait.for(f.short, type, go);
            var endTime = new Date().getTime();
            var t = (endTime - startTime)/1000 + 's';
            res.end('{"id":"'+rrr+'", "time":"' + t + '"}');
        } else {
            res.status(500).end('Wrong data!')
        }
    }

    this.short = short;
    function short (type, go, extra, user) { // extra - some extra data to be saved with the link, e.g. for paypal: {fsm:"FsmThatProcessesPayment"}
        var rrr = '----';
        if (go != undefined && go!="") {
            rrr = wait.for(shortify, type, go, extra, user);
        }
        return rrr;
    }

    this.shortify = shortify;
    function shortify (type, go, extra, user, callback) {
        var c = db.collection("links");
        //console.log("Testing global var. user: %j", global.user);
        var self = this;
        function getId (err, docs) {
            var id;
            if (docs == null || docs == undefined || docs.length<1) {
                while (true) { // iterating until generated id is defferent to what is in DB
                    id = makeid(4);
                    self.result = id;
                    console.log("new id: "+id+", extra: %j", extra);
                    c.update({id:id}, {id:id, type: type, redirect: go, user: user || global.user, extra: extra, created: new Date()}, {upsert:true}, function(err, docs) {
                        // TODO: remove it and put smth useful here.
                        c.count(function(err, count) {
                            console.log(format("count = %s, id=%s", count, id));
                        });
                    });
                    break;
                }
            } else {
                id = docs[0].id;
                self.result = id;
                console.log("Existing redirect! "+docs[0].id); // TODO: update adding time
            }
            console.log("id: "+id+", result: "+self.result);
            if (callback != null && callback != undefined) {
                //console.log("Callback called with: err: null, data: "+id+", callback: "+callback);
                callback(null, id); // err = null, data = id
            } else {
                console.log("Callback is null!");
            }
        }
        const searchObj = {type: type, redirect: go, extra: extra, user: user || global.user};
        console.log("Looking for supposedly existing redirect: ", searchObj);
        return c.find(searchObj).toArray(getId);
    }

    this.update = update; // updates link only
    function update(id, go, extra) { 
        var c = db.collection("links");
        var u = {};
        if (go) {
            u.redirect = go;
            console.log("Update go set! ", go);
        }
        if (extra) {
            u.extra = extra;
        }
        let result = new Promise ((resolve, reject) => {
            c.update({id}, {"$set":u}, function(err, docs) {
                console.log(format("Updated with new extra & go: %j, id %s", u, id));
                console.log(format("Updated results. Docs: %j, err: %s", docs, err));
                if (err) {
                    reject (err);
                } else {
                    resolve(docs);
                }
            });
        });
        return result;
    }

    this.get = get; // returns the record by ID
    function get(id) { 
        var c = db.collection("links");
        var record = wait.for(f_find, id);
        function f_find (id, callback) {
            c.findOne({id}, callback);
        }
        //console.log("With id: %s found: %j", id, record);
        if (!record) {
            record = {};
        }
        return record;
    }
}

module.exports = redirect;

//   db.close();
