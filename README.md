# Dockerize
Build:

`docker build -t nodevm .`

Tag for pushing to the registry:

`docker tag nodevm registry1.chabrum.com/nodevm`

Push to the registry:

`docker push registry1.chabrum.com/nodevm`

Stand-alone run

`docker run --network chabrum --hostname nodevm --rm --name nodevm registry1.chabrum.com/nodevm`
