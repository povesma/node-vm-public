//var parser = new xml2js.Parser();
var xml2js = require('xml2js');
var parseString = require('xml2js').parseString;
fs.readFile('/tmp/ctoc.xml', 'utf8', function(err,data) {
console.log(data.length);
var t = '';
parseString(data, {tagNameProcessors: [xml2js.processors.stripPrefix], explicitArray: false}, function (err, result) {
    console.log("ERR: "+err);
    //console.log("JSON: "+JSON.stringify(result));
	function proc(key,value) {
	    //console.log("pair: "+key + " : "+value+", type: "+typeof(value));
	    if (key =='coordinates') {
	      try{
	        value = JSON.parse(value);
	      }  catch (e) {
	      	console.log("Error: "+e);
	      }
	    }
	}

	function traverse(o,func) {
	    for (var i in o) {
	        //func.apply(this,[i,o[i]]);
	        //console.log("Type of %s: %s", i, typeof(o[i]));
		    if (i =='coordinates') {
		      try{
		        o[i] = JSON.parse(o[i]);
		      }  catch (e) {
		      	console.log("Error: "+e);
		      }
		    }

	        if (o[i] !== null && (typeof(o[i])=="object" || typeof(o[i])=="array")) {
	            //going on step down in the object tree!!
	            traverse(o[i],func);
	        }
		    if (i =='x' || i == 'y') {
		      o[i] = parseFloat(o[i])+0.0;
		    }

	    }
	}
	traverse(result,proc);
	result = result.getTmpsResponse.tmpsResult;

    result = JSON.stringify(result, null, 2);
    result = result.replace('"$"','"value"');
    fs.writeFile('/tmp/ctoc.json', result, function(err,data) {
	    console.log("Write ERR: "+err);
    	});
});
});


j = {"coordinates":[[{\"x\":\"-43.528684520694384\",\"y\":\"172.63851642608643\"},{\"x\":\"-43.52880509080192\",\"y\":\"172.63853251934052\"},{\"x\":\"-43.52880120144738\",\"y\":\"172.6383662223816\"},{\"x\":\"-43.52925625422733\",\"y\":\"172.63837158679962\"},{\"x\":\"-43.52926792220218\",\"y\":\"172.63785123825073\"},{\"x\":\"-43.5298202037634\",\"y\":\"172.6378619670868\"},{\"x\":\"-43.529808535895384\",\"y\":\"172.638258934021\"},{\"x\":\"-43.52991743590909\",\"y\":\"172.63828575611115\"},{\"x\":\"-43.529901878776315\",\"y\":\"172.63684272766113\"},{\"x\":\"-43.52978131086123\",\"y\":\"172.6368373632431\"},{\"x\":\"-43.529789089443675\",\"y\":\"172.63751864433289\"},{\"x\":\"-43.529279590174774\",\"y\":\"172.63751864433289\"},{\"x\":\"-43.529283479498474\",\"y\":\"172.63762593269348\"},{\"x\":\"-43.52881286951026\",\"y\":\"172.63761520385742\"},{\"x\":\"-43.52880509080192\",\"y\":\"172.6371055841446\"},{\"x\":\"-43.528684520694384\",\"y\":\"172.6370894908905\"}]]};



function findEl(o,tag, name) {
    for (var i in o) {
        console.log("Element: %s, type: %s",i, typeof(o[i]));
        if (typeof(o[i])=="object" || typeof(o[i])=="array") {
	        console.log("Checking object under index: "+i);
            //going on step down in the object tree!!
            e = findEl(o[i],tag, name);
            if (e!= null) {
            	return e;
            }
        } else {
	        console.log("Checking directly: "+i+" value: "+o[i]);
		    if (i ==tag && o[i] == name) {
		      return o;
		    }
		}
    }
    return null;
}



{"div":{"$":{"id":"tracking-new-results-basic"},"h2":[{"_":"Item delivery status for LW108974819NZ",
"$":{"name":"result-1"}}],"div":[{"$":{"class":"messages error"},"h2":["There was a problem"]}]}}
