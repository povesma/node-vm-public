var wait = require('wait.for');
var format = require('util').format;
var config_reader = require('config');
var config = {};
var top_config = {};
try {
  top_config = config_reader.get('config');
  config = config_reader.get('config.paypal');
} catch (e) {
  // oops. Load defaults? Quit?
}

var isSandbox = config.isSandbox;
var instance = config.instance;
var timer_url = top_config.timer_url;
var gateway_url = top_config.gateway_url;
var cfg = {};

try {
  cfg = config_reader.get('config.paypal.'+instance);
  console.log("Instance config loaded: %j",cfg);
} catch (e) {
  console.log("Config: error loading instance: "+instance);
  // return; TODO: Throw exception?
  // oops. Load defaults? Quit?
}
var paypalURL = cfg.url;
var paypalBusiness = cfg.email;
var sandbox_tag = '';
if (isSandbox) {
	sandbox_tag = '<input type="hidden" name="env" value="sandbox">';
}

var db_paypal = null;
var db_bank = null;

module.exports = Paypal;
function Paypal () {

	var redirector;
	this.setRedirector = setRedirector;
	function setRedirector (r) {
		redirector = r;
	}

	this.notification = notification;
	function notification (req, res) {

		console.log("Notification POST JSON: %s, body: %j", req.params, req.body);
		var orig_query = "";
		var json_body = {}
		if (typeof(req.headers) == 'object') {
			for (ind in req.headers) {
				console.log("Header: %s, value: %s", ind, req.headers[ind]);
			}
		}	
		if (typeof(req.body) == 'object') {
			for (ind in req.body) {
				console.log("Key: %s, value: %s", ind, req.body[ind]);
				orig_query += (orig_query.length>0?"&":"") + encodeURIComponent(ind)+"="+encodeURIComponent(req.body[ind]);
			}
			json_body = req.body;
		} else {
			orig_query = req.body;
			var g = require('query-string');
			json_body = g.parse(req.body);
			for (ind in json_body) {
				console.log("Processed Key: %s, value: %s", ind, json_body[ind]);
			}
		}
		pp_query = req.body;
		pp_query['cmd'] = '_notify-validate';
		var payload = "cmd=_notify-validate&" + orig_query;
		if (payload.indexOf("%20")>-1) { // Spaces are not URL-encoded
			payload = payload.replace(/\+/g, "%2B");	 // magic?!
		}
		console.log("Orig_request: "+payload+", going to verify via:"+paypalURL);
		res.end("All good!\n"+payload+"\n");

		var request = require('request');

		request.post(paypalURL, {body:payload}, function (error, response, body) {
		    wait.launchFiber(verifyPayment, error, response, body);
		});	

		function verifyPayment (error, response, body) {
        //if (!error && response.statusCode == 200) {
            console.log("Requested: %s, PP response: %s, code: %s, error: %s", paypalURL, body, response.statusCode, error);
            if (body == "VERIFIED") { // That's a genuine PayPal request
            	// put the record into the database
            	// Credit user's account
            	//initDb();
            	pp_coll = db_paypal.collection("paypal");
            	pp_coll.insert({"ipn":json_body, "headers":req.headers});
            	// userid is stored in "custom" field, passed to paypay.
            	// custom is a stringified json: {"chatneyID":"20420-2398-2930", "app":"application_name(FSM_name) to be called on payment"}
            	// TODO: improve custom data
            	// TODO: encrypt / hash "custom" data - store all in DB instead of sending to PP?
            	// TODO: Add timestamp
            	var user = {};
            	var record = {};
            	var fsm = null;
            	try {
            		custom = json_body.custom; // custom countains URL id
	            	console.log(" Custom: %s", custom);
	            	if (redirector != undefined) {
		            	record = wait.for(redirector.get, custom);
		            	user = record.user;
		            	if (record.extra != undefined && record.extra.fsm != undefined) {
		            		fsm = record.extra.fsm;
		            		delete record.extra['fsm'];
		            	}
		            }
		            accountCredit(user.chatneyID, json_body.mc_currency, Number(json_body.mc_gross));
	            } catch (e) {
	            	console.log(e);
	            }

	            // Start FSM, which is responsible for payments
	            // Create a timer event, that runs FSM right now
	            // {"finish":"in 1000 years","data":{"IdeaGrabber":{"ideaText":"${global.idea.text}","saved":"True"},"user":{"chatneyID":"${global.user.chatneyID}"}}}
	            if (user.chatneyID != undefined) {
		            var payload = {"finish":"now","data":{"namespace":fsm,"type":"ForkFSM",
		                          "params":{"paypal":json_body, "extra":record.extra},"user":user}, 
		                           callback:gateway_url + "/manager/event/fire", callerKey:"dev"}
		            var timerUrl = timer_url + "/api/setEvent";
					request.post(timerUrl, {body:JSON.stringify(payload), headers:{"Content-type": "application/json"}},
					    function (error, response, body) {
					    	//
					    	console.log("Timer result: %j, error: %s", body, error);
					  	}
					);
				}

            }
        //}
    }

	}
	this.accountCredit = accountCredit;
    function accountCredit(chatneyID, currency, amount) { // TODO: return balance
		account_coll = db_bank.collection("account");
		account_coll.update({"userid":chatneyID, "currency":currency},{$inc:{"balance":amount} },  {upsert:true});
		// TODO: transaction log!
	}
	this.accountDebit = accountDebit;
    function accountDebit(chatneyID, currency, amount) {
		account_coll = db_bank.collection("account");
		account_coll.update({"userid":chatneyID, "currency":currency},{$inc:{"balance":0-amount} },  {upsert:true});
		console.log("Account ", chatneyID,"debited for ", amount, currency);
	}


	this.initDb =initDb;

	function initDb (dbconn_paypal, dbconn_bank) {
	    var MongoClient = require('mongodb').MongoClient
	    var assert = require('assert');
	    //TODO: optimize many DB connections!
	    if (dbconn_paypal == null) {
	        // Connection URL
	        var url = 'mongodb://localhost/paypal';
	        // Use connect method to connect to the server
	        dbconn = wait.for(MongoClient.connect,url); 
	        if (dbconn != null) {
	            console.log("Paypal: New DB connection made to "+url);
	            db_paypal = dbconn;
	        }
	    } else {
	    	db_paypal = dbconn_paypal;
	    }

	    if (dbconn_bank == null) {
	        // Connection URL
	        var url = 'mongodb://localhost/bank';
	        // Use connect method to connect to the server
	        dbconn = wait.for(MongoClient.connect,url); 
	        if (dbconn != null) {
	            console.log("Bank: New DB connection made to "+url);
	            db_bank = dbconn;
	        }
	    } else {
	    	db_bank = dbconn_bank;
	    }
	}

	this.paymentLink = paymentLink;
	function paymentLink (req, res) { // creates a PayPal "Pay now" button and turns it into a short ID (to make a link)
		var empty = {'status':'error', 'message':'empty data'};
		if (typeof(req.body) == 'object') {
			var goodsName = 'Account top up';
			var goodsQuantity = 1;
			var goodsCode = "";
			var amount = 10;
			var shipping = 0;
			var currency = 'NZD';
			var custom = {};
			var source = {}; // FSM entity which made this call, so it will be called on Paypal's IPN
	        if (req.body.goods != undefined) {
	        	goodsName = req.body.goods;
	        }
	        if (req.body.amount != undefined) {
	        	amount = req.body.amount;
	        }
	        if (req.body.quantity != undefined) {
	        	goodsQuantity = req.body.quantity;
	        }
	        if (req.body.currency != undefined) {
	        	currency = req.body.currency;
	        }
	        if (req.body.shipping != undefined) {
	        	shipping = req.body.shipping;
	        }
	        if (req.body.chatneyID != undefined) {
	        	custom.chatneyID = req.body.chatneyID;
	        }
	        if (req.body.fsm != undefined) { //fsm to launch after payment (unless we are already there)
	        	custom.fsm = req.body.fsm;
	        }

			// convert the form into the link:
			id = getPaymentLink(goodsName, goodsCode, goodsQuantity, amount, currency, shipping, custom);
			if (id == null) {
	    		req.end (empty); // TODO: informative reply
			}
			res.end(JSON.stringify({id:id}));
		} else {
			res.end (JSON.stringify(empty));
		}
	}

	this.getPaymentLink = getPaymentLink;
	function getPaymentLink (goodsName, goodsCode, goodsQuantity, amount, currency, shipping, extra, user=null, hostbase=null) { // creates a PayPal "Pay now" button and turns it into a short ID (to make a link)
	       //Check mandatory fields
	        var returnUrl = cfg.returnUrl || '';
	        var cancelUrl = cfg.cancelUrl || '';
	        if (user && hostbase) {
	        	if (user.botWebURL) {
	        		if (config.return_html) {
		        		var returnHTML = format(config.return_html, user.botWebURL);
						var r_id = redirector.short('post', returnHTML, {});
						console.log("ReturnHTML ID generated: %s", r_id);
						returnUrl = hostbase + r_id;
					} else {
			        	console.log("Config return_html. Using default return & cancel URL: ", returnUrl, cancelUrl);
					}
					if (config.cancel_html) {
		        		var cancelHTML = format(config.cancel_html, user.botWebURL);
						var c_id = redirector.short('post', cancelHTML, {});
						console.log("CancelHTML ID generated: %s", c_id);
						cancelUrl = hostbase + c_id;
					}
	        	} else {
		        	console.log("No botWebURL. Using default return & cancel URL: %j", user);
	        	}
	        } else {
	        	console.log("No user or hostbase. Using default return & cancel URL: ", returnUrl, cancelUrl);
	        }
			var s = `<html>
<script async="async" src="https://www.paypalobjects.com/js/external/paypal-button.min.js?merchant=info-facilitator@talania.com"></script>
<center>
<body><font face="Arial">
<h1>Transferring to a PayPal secure page to complete this purchase...</h1>
<br><br><br>
<h2>Please tap the orange PayPal button if you are not transferred within 5 - 10 seconds. </h2><br>

			<form name="pp" method="post" action="%s" class="paypal-button" target="_top" style="opacity: 1;">

			<input type="hidden" name="business" value="%s">
			<input type="hidden" name="item_name" value="%s">
			<input type="hidden" name="item_number" value="%s">
			<input type="hidden" name="quantity" value="%s">
			<input type="hidden" name="amount" value="%s">
			<input type="hidden" name="currency_code" value="%s">
			<input type="hidden" name="shipping" value="%s">
			<input type="hidden" name="custom" value='%s'>`+
			((returnUrl)?('<input type="hidden" name="return" value="'+returnUrl+`">
    		<input type="hidden" name="cbt" value="Finish purchase">`):'')+
			((cancelUrl)?'input type="hidden" name="cancel_return" value="'+cancelUrl+'">':'')+
      
			`<input type="hidden" name="button" value="buynow">
			<input type="hidden" name="image_url" value="https://talania.com/wp-content/uploads/2016/04/talania_circle_75.png">
			<input type="hidden" name="tax" value="0">
			`+sandbox_tag+`
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="bn" value="JavaScriptButton_buynow">
			<button type="submit" class="paypal-button large">Buy Now</button></form>
</center></font></body>
			<!-- Automatic submission -->
			<script>pp.submit();</script></center></body>
			</html>`;

			// convert the form into the link:
			var id = redirector.short('post', "(tbr)", extra);
			console.log("Paypal ID generated: %s", id);
			if (id == '----') {
				return null;
			}
			s = format(s, paypalURL, paypalBusiness, goodsName, goodsCode, goodsQuantity, amount, currency, shipping, id);
			redirector.update(id, s);
			return id;
	}
}

function returnHTML (res) {
	res.end(cfg.paypal.return_html);
}

function cancelHTML (res) {
	res.end(cfg.paypal.cancel_html);
}
function getUrlBySocial (social) { // web messenger by social network name
	switch (social) {
		case 'telegram':
		case 'tgbot':
			return 'https://telegram.me/';
		case 'facebook':
		case 'fbbot':
			return 'https://m.me/';
		default:
			return null;
	}
	return null;
}