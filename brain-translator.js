var fs = require('fs');

var self = module.exports = {init, translate, langs};

var langs = [];
var vocab = [['_ZERO_'],['_ZERO_']];
var files = {}; // stores dicts
var src_words = {};
var trg_words = {};
var src_vocab = {};
// dictionaries will hold vocabulized versions of dictionaries, indexed by scope (bot name), and then hashes
var dictionaries = {};

const DO_NOT_TRANSLATE = '\n\n_-=DNT=-_';
// 
function init (languages = ['ru_en', 'ru_uk','src_ru', 'src_uk', 'src_en']) {
	langs = languages;
}

/*
** Translates phrases from one langugage to another
** if dictionary is given - uses it. dictionary if object:{hash: '', data: {ru:[],en:[],uk:[]}}
*/
function translate (s, lang_from='ru', lang_to='en', dictionary) { // dictionary = {data:{ru: [], uk: [], }, hash:"", scope: ""}
	if (lang_to == lang_from || !dictionary) {
		return s;
	}

	let fn = lang_from+'_'+lang_to;
	console.log('translate: langs: ', langs, ", from:",lang_from,", to:", lang_to);
	let i = langs.indexOf(fn);
	let v = vocabularaize(s, 0);
	//console.log('Vocabulized lower:', v);
	let v_upper = vocabularaize(s, 1);
	console.log('Vocabulized normal:', v_upper);
    var t_res = translate_simple(v, lang_from, lang_to, dictionary) || s;
    t_res = vocabularaize(t_res, 1).data.join(' ');
    let t = restoreString(t_res, v_upper.params);
    return t
}

function translate_simple (v, l_in='ru', l_out='en', dict_full) {
	let fn_src = l_in+'.txt';
	let src = null;
	let trg = null;
	//let dictionary = dict_full?dict_full.data:null;
	if (dict_full) { // TODO: think how to remove unused dictionaries
		console.log('BrainTranslator: using user dictionary hash:', dict_full.hash, ', scope:', dict_full.scope);
		if (dictionaries[dict_full.scope]) {
			if (dictionaries[dict_full.scope][dict_full.hash]) {
				let dict = dictionaries[dict_full.scope][dict_full.hash];
				src = dict[l_in];
				trg = dict[l_out];
				console.log('Using cached dictionary for pair:', l_in, l_out);
			} else { //  if specified hash not found, that mean that other hashes are not valid. Wipe them
				console.log('Cached dictionaries obsolete: ', Object.keys(dictionaries[dict_full.scope]), '. Purged old dict for scope:', dict_full.scope);
				dictionaries[dict_full.scope] = {};
			}
		} else {
			dictionaries[dict_full.scope] = {};
		}

		if (!src || !trg) {
			try {
				src = [];
				trg = [];
				dict_full.data.forEach(el => {
					src.push(el[l_in]); 
					if (!el['dnt']) { // if Do not translate flag not set - push translation
						trg.push(el[l_out]);
					} else {
						trg.push(DO_NOT_TRANSLATE);
					}
				});
				// cache the dictionary
				dictionaries[dict_full.scope][dict_full.hash] = {};
				dictionaries[dict_full.scope][dict_full.hash][l_in] = src;
				dictionaries[dict_full.scope][dict_full.hash][l_out] = trg;
				console.log('Created dictionary cache pair:', l_in, l_out);
			} catch (ex1) {
				console.log('User dictionar]y is damaged. Keep the original phrase without traslating it');
				return null;
			}
		}
		fn_src = dict_full.scope+'_'+dict_full.hash;
	} else {
		console.log('No User dictionary provided. Keep the original phrase without traslating it');
		return null;
	}
	if (!src) {
		src = src_words[fn_src];
	}
	if (!src) {
		src = []
	}
	if (!trg) {
		trg = []
	}
	// Read Default dictionary from the files
	/*
	if (!src) {
		src = readFile(fn_src).toLowerCase().replace(/\r/g,"").split("\n");
		src_words[fn_src] = src;
	}

	let fn_trg = l_out+'.txt';
	if (!trg){
		trg = trg_words[fn_trg];
	}
	if (!trg){
		trg = readFile(fn_trg).replace(/\r/g,"").split("\n");
		trg_words[fn_trg] = trg;
	}
	*/
	let t = v.data;
	if (!src_vocab[fn_src]) {
		src_vocab[fn_src] = [];
	}
//	console.log('translate_simple:  translating: ',t.join(' '));
	for (var i = 0; i < src.length; i++) {
		let vc = src_vocab[fn_src][i];
		if (!vc) {
			vc = vocabularaize(src[i], 0).data;	
			src_vocab[fn_src][i] = vc;
		}
			
		if (vc.length == t.length) {
			match = true;
			for (var k = 0; k < t.length; k++) {
//				console.log('translate_simple: ',t[k] ,' vs ', vc[k]);
				if (t[k] !=  vc[k]) {
					match = false;
					break;
				}
			}
			if (match) {
				console.log('translate_simple hit: ', trg[i], '[', i, ']')
				if (trg[i] == DO_NOT_TRANSLATE) {
					return src[i];
				} else {
					return trg[i];
				}
			}
		}
	}
	return null;

}


function readFile(fn) {
	if (files[fn]) {
		console.log("Hit cache for",fn);
		return files[fn];
	}
	let f = fs.readFileSync(fn, 'utf-8',function (err) {
		if(err) {
        	return console.log(err);
    	} else  {
    		console.log('The file ' +fn +' read!');
    	}
	});
	files[fn] = f;
	return f;
}

function toIndexes(s, t, len, direction) { // len - if >0, fill with zeroes to this length, 
											// direction: 0 - forward, 1 - reverse
	r = [];
	for (var i = 0; i < s.length; i++) {
		if (direction==0) {
			r.push(vocab[t].indexOf(s[i]));
		} else {
			r.unshift(vocab[t].indexOf(s[i]));
		}
	}
	if (len>0 && r.length<len) {
		while (r.length<len) {
			r.push(vocab[t].indexOf('_ZERO_'));
		}
	}
	return r;
}

function binarize(a, len) { // take the vector of indexes and turns it into a binary matrix of binary vectors
	m = [];
	for (var i = 0; i < a.length; i++) {
		let v = new Array(len);
		v.fill(0);
		v[a[i]] = 1;
		m.push(v);
	}
	return m;
}



// takes a sentence, splits it into words, returns array of words and makes a vocabulary
function vocabularaize (s, t) { // t = 0 for source or 1 target
	let r = {data:[], params:{number:[], url:[], var:[]}};
	let a = s;
	while(true) { // replacing variables with {var} and adding them to the var's pool
		mm = a.match(/\{\{(.+?)\}\}/);
		if (mm) {
			//console.log('Processing:', a, ', var:',mm);
			r.params.var.push(mm[1]);
			a = a.replace(mm[0], '{var}');
		} else {
			break;
		}
	}
	if (t == 0) {
		a = a.toLowerCase();
	}
	// TODO: do not split vars!
	if (false && a.length<=15 && a.indexOf('{')<0 && !a.match(/\d/)) { // short string with no vars and numbers in it
		a = a.split('');
	} else {
		a = a.split(' ');
	}
	for (var i = 0; i < a.length; i++) {
		throwout = "\r ";
		for (var j = 0; j < throwout.length; j++) {
			a[i] = a[i].split(throwout[j]).join('');
		}
		if (a[i].length==0) {
			continue;
		}

		if (a[i].startsWith('http://') || a[i].startsWith('https://')) {
			r.params.url.push(a[i]);
			a[i] = '{url}';
		}

		nm = a[i].match(/\d/);
		if (nm) {
			r.params.number.push(''+a[i]);
			//console.log('Number: ', a[i]);
			a[i] = '{number}';
		}

		if (t == 0) {
			throwout = "\r .,!?()/\\<>";
		}
		// remove unicode symbol
		if (a[i].charAt(0)>1024) {
			a[i] = a[i].split(a[i][0]).join('');
		}
		for (var j = 0; j < throwout.length; j++) {
			a[i] = a[i].split(throwout[j]).join('');
		}
	
		if (vocab[t].indexOf(a[i]) <0 ) {
			vocab[t].push(a[i]);
		}
		if (a[i].length>0) {
			r.data.push(a[i]);
		}
	}
	return r;
}

function restoreString(s, params) {
	console.log("Restoring string",s);
	counter = {};
	for (var i in params) {
		counter[i] = 0;
	}

	mm = s.replace(/\{(.+?)\}/g, (d,k,c,o,j)=>{ 
			if (params[k]) {
				counter[k]++;
				return params[k][counter[k]-1];
			} else {
				return "-????-";
			}

		});
	return mm;
}


function vectorizeSentence(s) {
	let vc = vocabularaize(s, 0);
	let v = vc.data;
	v = toIndexes(v, 0, max_src_sent, 1); // max_src_sent - maximum length of the sentence
	v = binarize(v, vocab_length);
	v = [].concat.apply([], v);
	return v;
}

function invert (arr) {
	let res = [];
	for (let i in arr) {
		res.push(1-arr[i]);
	}
	return res;
}

function readFromBinary (a, voc_size, sent_length, direction) { 0 - straigh, 1 - back
  // straigth
  let binary_word = [];
  let words = [];
  for (var i = 0; i < a.length; i++) {

  }
}
