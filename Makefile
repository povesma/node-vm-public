ALL:
	jx compile node-vm.jxp
#	strip -s ../bin/node-vm

prepare:
	@echo Preparing sources
	jx package node-vm.js ../bin/node-vm --native

test:
	@{ node node-vm.js & echo $$! >node-vm.pid; }
	@sleep 5s
	wget -O - --post-data='{"data":"hi","code":"data=data+\" you!\";"}' --header="Content-Type: application/json" http://localhost:1607/eval -q
	@echo
	wget -O - --post-data='{"data":"hi","code":"throw Error(\"boom!\");"}' --header="Content-Type: application/json" http://localhost:1607/eval -q
	@echo
	@kill `cat node-vm.pid` && rm node-vm.pid
