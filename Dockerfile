FROM node:carbon
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN npm audit fix
COPY . .
RUN echo "require ('canvas')" | node -
COPY ./config/default.json5.docker ./config/default.json5
CMD [ "node", "node-vm.js" ]
