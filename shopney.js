var wait = require('wait.for');
var format = require('util').format;
var config_reader = require('config');
var query_string = require('query-string');
var request = require('request');
var config = {};
var top_config = {};
try {
  top_config = config_reader.get('config');
  config = config_reader.get('config.shopney');
  console.log (format("config loaded: %j",config));
} catch (e) {
  // oops. Load defaults? Quit?
  console.log ("Troubles when loading config :-(", e);
}

var instance = config.instance; // not used now
var timer_url = top_config.timer_url;
var gateway_url = top_config.gateway_url;

module.exports = Shopney;
function Shopney () {

	var redirector;
	this.setRedirector = setRedirector;
	function setRedirector (r) {
		redirector = r;
	}

	this.granted = granted;
	function granted (req, res) { // called by FB when page admin permission is granted . call_id = stored data for this auth request
  //TODO: check if this call is really from FB
		console.log("FB Access granted Notification GET params: %j", req.query);

		var user = {};
		var fsm = '';
		var call_id = '';
		var pageid = '';
		var redirectUrl = '';
    	try {
    		if (!('state' in req.query || 'reload' in req.query)) {
    			var h_redir = `<html>
									<head>
									</head>
								  	<body>
								  		Please wait. Processing FB authentication.

									  <script>
									    console.log("Full fb URL: "+window.location.href);
									    var parts = window.location.href.split('#');
									    console.log('Split into: '+parts[0]+' - '+parts[1]);
									    if (parts.length>1 && parts[1].indexOf('state=')>=0) {
									    	window.location = parts[0]+parts[1]+'&reload=1';
									    } else {
									    	window.document.body.innerHTML = 'Wrong FB response. Please wait for start over.';
									    	parts = parts[0].split('?');
									    	window.location = parts[0]+'?reload=1';
									    }
									  </script>
								  	</body>
								</html>`;
				res.end(h_redir);
				console.log("Redirected with #anchor")
				return;
    		}
    		call_id = req.query.state;
        	if (redirector != undefined) {
            	record = wait.for(redirector.get, call_id);
            	user = record.user;
            	if (record.extra != undefined) {
            		for (key in record.extra) {
            			console.log ('Stored link key:',key,', n value: ', record.extra[key]);
            		}
            	    if(record.extra.fsm != undefined) {
	            		fsm = record.extra.fsm;
	            		delete record.extra['fsm'];
	            	}
            	    if(record.extra.pageid != undefined) {
	            		pageid = record.extra.pageid;
	            		delete record.extra['pageid'];
	            	}
            	    if(record.extra.fullurl != undefined) {
	            		redirectUrl = record.extra.fullurl;
	            		delete record.extra['fullurl'];
	            		console.log('FullUrl: ', redirectUrl);
	            	}
            	}
            }
        } catch (e) {
        	console.log(e);
        }

        if (req.query.code) {
        	// request a token based on this code
        	codeToToken(req.query.code, verifyToken);
        }

    	function codeToToken (code, callback=null) {
			var appid = config.appid;
			var thisUrl = redirectUrl;
			var appSecret = config.app_secret;
			var codeToTokenUrl = 'https://graph.facebook.com/v2.8/oauth/access_token?client_id='+
					appid+'&redirect_uri='+thisUrl+'&client_secret='+appSecret+'&code='+code;
    		console.log('TokenRequest URL: ', codeToTokenUrl);
			request.get(codeToTokenUrl, function (error, response, body) {
			    	console.log("CodeToToken body: %j", body);
			    	var access_token;
			    	try {
						var json_body = JSON.parse(body);
						for (ind in json_body) {
							console.log("Key in body: %s, value: %s", ind, json_body[ind]);
							if (ind == 'access_token') {
								access_token = json_body[ind]; // this must be a permanent token
							}
						}
			    	} catch(e) {

			    	}

			    	if (callback && access_token) {
			    		console.log("Calling callback from codeToToken");
			    		callback(error, access_token, runFsm);
			    	}
			  	}
			);
		}
		// https://graph.facebook.com/v2.8/oauth/access_token?client_id=** app_id ** &client_secret=** app_secret **&grant_type=client_credentials
		function verifyToken (error, token, callback=null) { // read information about the token
			// body...
			var appAccessToken = config.app_token;
			console.log("Verifying all about te token");
			var tokenVerifyUrl = 'https://graph.facebook.com/debug_token?input_token='+token+'&access_token='+appAccessToken;
			// response like:
			// {"data":{"app_id":"1707849486171092","application":"TalaniaBot","expires_at":0,"is_valid":true,"issued_at":1468411910,
			// "scopes":["pages_messaging","public_profile"],"user_id":"439715929749648"}}
			request.get(tokenVerifyUrl, function (error, response, body) {
			    	console.log("VerifyToken body: %j", body);
			    	var access_token;
			    	var scopes = [];
			    	try {
						var json_body = JSON.parse(body);
						for (ind in json_body.data) {
							console.log("Key in body: %s, value: %s", ind, json_body.data[ind]);
							if (ind == 'scopes') {
								for (el in json_body.data[ind]) {
									console.log("Permission: %s", json_body.data[ind][el]);
								}
								scopes = json_body.data.scopes;
							}
						}
			    	} catch(e) {
			    		console.log("Error when verifying a token: ", e);
			    	}

			    	if (callback && token && scopes) {
			    		callback(error, token, scopes);
			    	} else {
			    		res.send("Could not verify a token").status(400);
			    	}
			  	}
			);
		}

		function runFsm(error, token, scopes) {
	        if (user.chatneyID != undefined) {
	            var payload = {"finish":"now","data":{"namespace":fsm,"type":"ForkFSM",
	                          "params":{"event_name":"fbgrant", "extra":record.extra, 
	                          "fb":{"fbauth":req.query /*do we need it?*/,  "token":token, "scopes":scopes, "pageid":pageid}},
	                          "user":user}, callback:gateway_url + "/manager/event/fire", callerKey:"dev"}
	            var timerUrl = timer_url + "/api/setEvent";
				request.post(timerUrl, {body:JSON.stringify(payload), headers:{"Content-type": "application/json"}},
				    function (error, response, body) {
				    	//
				    	console.log("Timer result: %j, error: %s", body, error);
				    	if (error) {
				    		res.send(format("Error calling timer. call_id: %s, error: %j",call_id, error)).status(400);
				    	} else {
				    		res.end ("OK, go on with further setup in your messenger. Thanks.");
				    	}
				  	}
				);
			} else {
		    	console.log("User not found for this call_id: %s", call_id);
		    	res.send("No such redirect! "+call_id).status(400);
			}
		}
    }

}
