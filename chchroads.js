// 'use strict';
function chchroads () { // preloaded modules
    var assert = require('assert');
    var wait = require('wait.for');
    var format = require('util').format;

    this.initDb =initDb;
    var db = null;
    function initDb (dbconn) {
        if (dbconn == null) {
            var MongoClient = require('mongodb').MongoClient
            var assert = require('assert');
            // Connection URL
            var url = 'mongodb://localhost/redirects';
            // Use connect method to connect to the server
            dbconn = wait.for(MongoClient.connect,url); 
            if (dbconn != null) {
                console.log("New DB connection made to "+url);
                db = dbconn;
            }
        }
        process.env.TZ = 'Pacific/Auckland';
        //app.post('/go/:url', jsonParser, function (req, res) {
    }

    this.updateRoadStatus = updateRoadStatus;
    function updateRoadStatus (req, res) {
            var c = db.collection("links");
            var clicks = db.collection("clicks");
            console.log("Looking for "+req.params.id);
            c.find({id:req.params.id}).toArray(function(err, docs) {
                    assert(docs, null);
                    if (docs.length>0) {
                        docs.every(function (e,i) {
                            //TODO: if shortcut is short and not public - ask for authentication
                            if (e.redirect.length>5) {
                                if(e.type=="url") {
                                    // e.redirect = '<html><head><meta property="og:url" content="'+e.redirect+'"/>\n<meta http-equiv="refresh" content="1;url='+e.redirect+'"></head></html>'
                                    res.redirect(301, e.redirect);
                                } 
                                if(e.type=="refresh") {
                                    var to_send = '<html><head><meta property="og:url" content="'+e.redirect+'"/>\n<meta http-equiv="refresh" content="1;url='+e.redirect+'"></head></html>'
                                    res.end(to_send);
                                } 
                                if (e.type=="post") {// ... and leave post as it is
                                    res.end(e.redirect);
                                }
                                var headers = req.headers;
                                headers.cookies = req.cookies;
                                headers.hostname = req.hostname;
                                headers.ip = req.ip;
                                headers.ips = req.ips; // proxy delivered IPs
                                headers.method = req.method;
                                headers.ip = req.ip;

                                console.log(format("Headers: %j, ", headers));
                                clicks.insert({"id":req.params.id, "type":e.type, "link":e.redirect, "headers":headers, "timestamp":new Date().getTime(), "time":new Date()});
                                // TODO: record headers to DB
                                console.log(format("%s redirected to %s", req.params.id, e.redirect));
                            } else {
                                console.log("Wrong redirect entry for this ("+req.params.id+") id");
                                res.status(404).send("Pff. Wrong URL.");
                            }
                        });
                    } else {
                        console.log("Redirect entry not found for this ("+req.params.id+") id");
                        res.status(404).send("Wrong shortcut.");
                    }
            });

    }

    this.getRoadInfo = getRoadInfo;

    function getRoadInfo (req, res) {
        var f = {short:shortify, result: 'empty'};
        var location = req.body.location; // {"lat":111, "lon":111}
        var lat = location.lat;
        var lon = location.lon;
        var distance = req.body.distance; //  in meters
        var all = req.body.all; // if all = 1 - then load all!
        var c = db.collection("spots");
        c.find({ geometry: {$near: {$geometry: {type: "Point" ,coordinates: [ lon, lat ] },
            $maxDistance: distance, $minDistance: 0}}}).toArray(function (err, docs) {

            });
    }
}

module.exports = chchroads;

//   db.close();