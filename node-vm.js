require('sugar');
const format = require('util').format;
var vm = require('vm');
var wait = require('wait.for');
var express = require('express');
var cookieParser = require('cookie-parser')
const rd = require('./redirector.js');


var config_reader = require('config');
var config = {};
var translate = require('./brain-translator.js')
translate.init();

////// MANUALLY REQUIRED ////////
const Buffer = require('buffer')
var console = require('console');
const dateformat = require("dateformat")
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const moment = require("moment");
const CryptoJS = require("crypto-js");
const cryptolib = require('crypto');
var uuid = require("uuid");
var pugRender = require("pug").render;
var transliteration =  require('transliteration');
var URL = require('url');

/////////////////////////////////

var redirDb = null;
var contexts = {}; 
var caches = {}; 
// GC contexts garbage collect every 30 sec
setInterval(() => {
  let ts = new Date().getTime() - 30000; // 30 sec GC timeout
  let ks = Object.keys(contexts);
  let l = ks.length;
  for (let k of ks) {
    let el = contexts[k];
    if (! (el && el.ts && el.ts>ts)) {
      console.log("contexts GC: removed",k);
      delete contexts[k];
    }
  }
}, 30000);

var request = require('request');
var Canvas = require('canvas');
var Image = Canvas.Image;

var tz = null;

var mongoUrl = null;
const defaultTZ = 'GMT'; //'Pacific/Auckland';
try {
  config_reader.get('config');
  config = config_reader.get('config');
  try {
    tz = config_reader.get('TZ')
  } catch (e2) {
    try {
      tz = config_reader.get('tz')
    } catch (e3) {
      console.log('TZ/tz not found in config. Using default: ' + defaultTZ);
    }
  }
} catch (e) {
  // oops. Load defaults? Quit?
  console.log('Config reading error!', e);
}
if (!tz) {
  tz = config.has('TZ') ? config.get('TZ') : defaultTZ;
}

var QrCode = null;

if (config && config.qr) { // QR code reading should be explicitely enabled in config!
  try {
    QrCode_ = require('qrcode-reader');
    QrCode = new QrCode_();
  } catch (e1) {
    console.log('Error loading qrcode-reader:', e1.message);
  }
}

if (config && config.mongodb_url) {
  mongoUrl = config.mongodb_url;
  console.log('mongodb_url set to', mongoUrl);
} else {
  console.log('mongodb_url is not defined in config! Exitting');
  process.exit(3);
}

// wait.launchFiber(new rd(null, mongoUrl, tz).initDb, redirDb); // Why do we need it?
// wait.launchFiber(new paypal().initDb, null);

var app = express();
app.use(cookieParser());
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json({ limit:'60MB' ,extended: false });
console.log('Config: ', config);
process.env.TZ = tz || defaultTZ; // TODO: to config!

function redirectorObj(redirDb, glob) {
  this.redirector = null;
  this.redirDb = undefined;
  this.test = 8;
  console.log('THIS test:', this.test);
  function createRedirector() {
    console.log("this.redirector: ", this.redirector, this.test);
    this.test = 9999;
    if (!this.redirector) {
      console.log('Creating redirector: ', redirDb);
      const _redirector = new rd(glob, mongoUrl, tz);
      console.log("BEFORE this.redirDb:", this.redirDb);
      this.redirDb = _redirector.initDb(redirDb || this.redirDb);
      console.log("AFTER this.redirDb:", this.redirDb);
      this.redirector = _redirector;
    }
    return this.redirector;
  }

  this.short = (type, go, extra, user) => {
    console.log('Calling short: ', this.redirDb, type, go, extra, user);
    return createRedirector().short(type, go, extra, user);
  }
  this.redirUpdate = () => {
    return createRedirector().update(...arguments);
  }
  this.redirGet = () => {
    return createRedirector().get(...arguments);
  }
}

const redirector = new redirectorObj(redirDb, {}); // {} replaces global

function postHandle(req, res) {
    var caller = req.body.caller ? req.body.caller : {};
    var sandbox;

    function Callback(g, d) { // object
      this.performed = false;
      this.g = g;
      this.d = d;
    }
    Callback.prototype.setPerformed = function(performed=true) { 
      this.performed = performed;
    }
    Callback.prototype.finished = function(g, d, update=true) { // global and data respectively, if update: merge current g & d with new, if !update - replace
      this.performed = true;
      console.log("== Finished ==");
      var en = new Date();
      const __runnning_time = en.valueOf() - st.valueOf();
      if (update) { // update this values with new ones
        if (g) {
          Object.assign(this.g, g);
        }
        if (d) {
          Object.assign(this.d, d);
        }
      } else { // replace initial values
        if (g) {
          this.g = g; //Object.assign({}, g);
        }
        if (d) {
          this.d = d; //Object.assign({}, d);
        }
      }
      response = {"data": this.d, "global": this.g};
      if (! response["data"]) {
        response["data"] = {};
      }
      sandbox = null;
      response["status"]="success";
      if (response["data"]["status"]) { // if JS uses status var - we report status to _status
        response["data"]["_status"] ="success";
      } else {
        response["data"]["status"] ="success";
      }
      console.log("before end(response)");
      end(response);
      en = new Date();
      const __total_runnning_time = en.valueOf() - st.valueOf();
      console.log("Code by <"+caller.name+ "> done ok in "+__runnning_time+" ms (TOTAL: "+__total_runnning_time+")\n\n");
    }
    if (caller.instance) {
      caches[caller.instance] = caches[caller.instance] || {}; // initilize as empty object is not exists
    }
    var gl = req.body.global || {};
    var dt = req.body.data || {};
    var Utils = {findEl};

    var call_back = new Callback(gl, dt);
    function getCallback(gl_, dt_) {
      return new Callback(gl_, dt_);
    }
    function setCache(value) { // TODO: use REDIS to store it!
      // TODO: Check size of the further object
      try {
        if (typeof(value) =='object' && caches[caller.instance] 
            && typeof(caches[caller.instance]) =='object') {
          Object.assign(caches[caller.instance], value);
          return 'merged';
        } else {
          caches[caller.instance] = value;
          return 'set';
        }
      } catch (e) {
        console.log('setCache exception:',e);
        return 'fail'
      }
    }
    sandbox = {
      //data: dt, //(req.body.data!=undefined?req.body.data:{}),
      //global: gl,
      caller: (caller),
      sugar: function (text) {
        return Date.create(text);
      },
      cache: caches[caller.instance] || {}, // useless if no instance is given
      setCache: setCache,
      dateformat,
      phoneUtil,
      moment,
      Buffer: Buffer,
      Utils: Utils,
      console: console,
      uuid: uuid,
      pugRender,
      URL,
      CryptoJS: CryptoJS,
      cryptolib: cryptolib,
      redirector: function (type, go, extra) {
        return redirector.short(type, go, extra, gl.user)
      },
      redirUpdate: redirector.update,
      redirGet: redirector.get,
      translate: translate.translate,
      Round: function (n, pos) {
        return parseFloat(Math.round(n + 'e+'+pos)  + 'e-'+pos);
      },
      transliteration: transliteration, // transliteration.transliterate and transliteration.slugify
      QrRead: readqr,
      Callback: call_back,
      CB: getCallback // not used not. Test it!
    };
    let ts = new Date().getTime();
    console.log("contexts len:",  Object.keys(contexts).length);
    var st = new Date();
    var response = new Object();
    if (req.body.code!=undefined){
      try {
        var no_context_cache = req.body.code.includes('##NO_CONTEXT_CACHE');
        var return_via_callback = req.body.code.includes('##RETURN_VIA_CALLBACK');
        console.log("\nStarting code from <"+caller.name+ "> at", st,', no context cache:', no_context_cache, ', instance',caller.instance);

        let context = gl.idKey?contexts[gl.idKey]:undefined;
        if (context && !no_context_cache) {
          console.log("Got cached context @ "+context.ts);
          contexts[gl.idKey].ts = ts;
          context = context.context;
        } else {
          context = vm.createContext(sandbox);
          console.log('New context created!');
          if (gl.idKey) {
            contexts[gl.idKey] = {context:context, ts: ts}; // store time for rufther GC
          }
        }

        //console.log(req.body);
        //let res = vm.runInNewContext('global = '+JSON.stringify(gl)+'; data = '+JSON.stringify(dt)+'\n'+req.body.code, sandbox, {timeout: 3500}) || {};
        delete sandbox.callback_performed;
        //call_back = new Callback(gl, dt);
        let j_s = JSON.stringify(dt, null, 2);
        if (!j_s.includes('{') || j_s == '"'+dt+'"') {
          j_s = '"'+dt+'"';
        }
        //context.global = gl;
        //context.data = dt;
        let res = vm.runInContext('global = '+JSON.stringify(gl, null, 2)+'; \ndata = '+j_s+';\n'+req.body.code, context, {timeout: 3500}) || {};
        //let res = vm.runInContext(req.body.code, context, {timeout: 3500}) || {};
        sandbox = context;
        console.log('return_via_callback:',return_via_callback);
        if (!return_via_callback) {
          if (Object.getPrototypeOf(res) == Promise.prototype) {
            res.then ((p) => { // p is always data (the last operation in JS should return / set data var)
              console.log('Promise JS server response');
              call_back.finished(sandbox.global, p, false);
            }).catch ((e) => {
              console.log('Error JS server response');
              call_back.finished(sandbox.global, e, false);
            })
          } else { // regular answer
            console.log('Regular JS server response');
            call_back.finished(sandbox.global, sandbox.data ,false);
          }
        } else {
          console.log('Result has been delivered via callback');
        } // prepare and run JS code. TODO: ==> Amazon Lambda?
      } catch(e) {
        console.log(e);
        console.log(req.body);
        response = {data:
                      {
                        status:"fail"
                      },
                      status: "fail", 
                      message: "Your code is shit, sir!",
                      error: e.stack,
                      payload: req.body
                  };
        end(response);
      }
    }

    function end(response) {
      try {
        res.end(JSON.stringify(response));
      } catch (e) {
        res.end(JSON.stringify({"error": e.toString()}));
      }
    }
}


const promBundle = require('express-prom-bundle');
const promClient = require('prom-client');

function setLabels(labels, req, res) {
  // delete default values
  delete labels.caller_instance;
  delete labels.caller_name;
  delete labels.caller_namespace;
  delete labels.caller_node;

  if (req.body === Object(req.body)) { // it's a real object
    const caller = req.body.caller;
    if (caller === Object(caller)) {
      caller.instance ? labels.caller_namespace = caller.instance : '';
      caller.name ? labels.caller_name = caller.name : '';
      caller.namespace ? labels.caller_namespace = caller.namespace : '';
      caller.node ? labels.caller_node=caller.node : '';
    } 
  }
}


const bundle = promBundle({
  buckets: [0.1, 0.25, 0.5, 0.7, 0.95],
  includeMethod: true,
  includePath: true,
  customLabels: {
    caller_instance: undefined,
    caller_name: undefined,
    caller_namespace: undefined,
    caller_node: undefined
  },
  transformLabels: setLabels,
  promClient: {
    collectDefaultMetrics: {
      timeout: 1000
    }
  }
});

const qrRequestsCount = new promClient.Counter({name: 'nodevm_qrcount', 
          help: 'Number of QR recognintion requests', labelNames: ['success']});

const restartsCount = new promClient.Counter({name: 'nodevm_restarts', 
          help: 'Number of terminations / restarts'});

app.use(bundle);

// TODO: move redirects to a separate service / completely rework
// How it works now:
// in JS user calls:
// id = redirector(type, url, any_extra_data); and sends urls like
// type: url, post, refresh, proxy
// https://our_host/go/id to the web user
// web user goes to this url, after parsing we get id, and can resolve them to original data:
// data = redirGet(id)
// data.user - user, on whos behalf we called redir()
// data.extra - our arbitrary data
// data.redirect - initial url, data.type - initial type
// also "update" is available. TODO: describe how it works
app.get('/go/:id', new rd(null, mongoUrl, tz).redirect); // used for redirects
app.get('/go/:id/*', new rd(null, mongoUrl, tz).redirect); // used for redirects

app.post('/eval', jsonParser, function (req, res) {
  wait.launchFiber(postHandle, req, res);
});

function readqr (url, resp=null, callback = null) {
  let image = null;
  if (!url) {
    response = {'status': 'fail', "error": 'Oh, Bad. No URL in post'};
    qrRequestsCount.labels('bad').inc();
    finalize();
    return;
  }
  request.get({ url: url, encoding: null }, function(err, res, body) {
      if (err) {
        response = {'status': 'fail', "error": "Failed loading URL, " + err.toString()};
        qrRequestsCount.labels('bad').inc();
        finalize();
        return;
      }

      image = new Image();

      image.onerror = function() {
          response = {'status': 'fail', "error": "Not an image on this URL, " + arguments.toString()};
          console.error(arguments);
          qrRequestsCount.labels('bad').inc();
          finalize();
      };

      image.onload = function() {
          console.log('loaded image ', url);
          canvas = new Canvas(image.width, image.height);
          ctx = canvas.getContext('2d');
          ctx.drawImage(image, 0, 0, image.width, image.height);
          var dt = ctx.getImageData(0, 0, image.width, image.height);
          QrCode.callback = (error, text)=> {
            if (error) {
              response = {'status':'fail', "error": "No QR code on the image, " + error};
              qrRequestsCount.labels('fail').inc();
              finalize();
            } else {
              response = {'status':'success','text':text};
              qrRequestsCount.labels('ok').inc();
              finalize();
            }
          }
          QrCode.decode(dt);
      };
      image.src = new Buffer(body, 'binary');
  });

  function finalize() {
    console.log(callback, resp, url);
    if (callback) {
      try {
        callback(response);
      } catch (e) {
          console.error(e, e.stack);
      }
    } else {
      if (resp) {
        resp.end(JSON.stringify(response));
      }
    }
  }
}

//test
//cbb = (r)=>{global.text = r.text; console.log(r);};
//readqr('https://scontent.xx.fbcdn.net/v/t35.0-12/20273175_724511217732842_1795382828_o.jpg?_nc_ad=z-m&oh=9510fdc61edcfdea10b36e7137cbd338&oe=5973C2C8', null, callback=cbb);

app.post('/readqr', jsonParser, function (req, res) {
  if (QrCode) {
    if (req.body && req.body.url) {
      let url = req.body.url;
      readqr(url, res=res, callback=null);
    } else {
      res.end("{'status':'fail', 'error':'no url in post'}");
    }
  } else {
      res.end("{'status':'fail', 'error':'QR code reading not enabled in this installation'}");
  }
});

// if started in Docker, host should be 0.0.0.0
var server = app.listen(1607, config.host?config.host:'localhost', function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("NodeVM listening at http://%s:%s", host, port)
});

process.on('SIGINT', function() {
    restartsCount.inc();
    process.exit();
});

app.disable('x-powered-by');

function findEl(o,tag, name) {
    for (var i in o) {
        // console.log("Element: %s, type: %s",i, typeof(o[i]));
        if (typeof(o[i])=="object" || typeof(o[i])=="array") {
          // console.log("Checking object under index: "+i);
            //going on step down in the object tree!!
            e = findEl(o[i],tag, name);
            if (e!= null) {
              return e;
            }
        } else {
          // console.log("Checking directly: "+i+" value: "+o[i]);
        if (i ==tag && o[i] == name) {
          return o;
        }
    }
    }
    return null;
}

